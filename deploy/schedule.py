#!/usr/bin/python3
import pickle as pk
import numpy as np
import pandas as pd
import time
import math
import itertools
import statistics
import random
import sys
sys.path.append('../commands')

import kube
import serf
import deployment_files as deploy

def DfPrint(df):
	print(pd.DataFrame(df))

def PickleLoad(filename):
	"""
	Load the saved pk files
	"""
	pickledlist=[]
	with open("/tmp/"+filename, 'rb') as f:
		pickledlist = pk.load(f)
	return pickledlist

def GetSortedNodes(latencies):
	"""
	Returns a list of Sorted Nodes
	"""
	Nodes=[]
	for k,v in latencies.items():
		Nodes.append(k)
	Nodes=sorted(Nodes)
	return Nodes

def GetLatency(Node1,Node2,latencies):
	"""
	Returns the latency between Node1 and Node2
	"""
	for k,v in latencies[Node1]: 
		if k==Node2: 
			return v 
	print("latency not found, Returning 0")
	return 0


def GetLatMat(latencies,Nodes):
	"""
	Create a Latency ma as np array
	""" 
	l=[]
	for node1 in Nodes:
		temp=[]
		for node2 in Nodes:
			temp.append(GetLatency(node1,node2,latencies))
		l.append(temp)
	LatMat=np.array(l)
	return LatMat

def CalculateF(x,B=1,index=1):
	"""
	Calculate the function defined in Proxy-mity:
		x			=> The latency between the two tested nodes
		B 			=> The value Beta in Proxy-mity equation FLOAT ]0,1]
		index		=> Decides what function to be used to calculate function F INT {1,2,3}

	"""
	assert x>0, "the latency value (%r) is negative or zero" % x
	if index==1: # Choose the Exponential function
		return math.exp(-B*x)
	if index==2: # Choose the proportional function
		return 1/(B*x)
	if index==3: # Choose the Degree function
		return 1/math.pow(x,B)

def GetProbMat(latencies,Nodes,B=1,index=1):
	"""
	Create a unified Probability that will be used with all the selected placement: 
		latencies 	=> Inter-node Latencies DICT
		Nodes 		=> list of SORTED nodes names LIST OF STRINGS
		B 			=> The value Beta in Proxy-mity equation FLOAT ]0,1]
		index		=> Decides what function to be used to calculate function F INT {1,2,3}
	"""
	ProbMat=[]
	for node1 in Nodes: 
		temp=[]
		sumF=0
		for node2 in Nodes: 
			l=GetLatency(node1,node2,latencies)
			#Apply proxy-mity Routing function
			F=CalculateF(l,B=B,index=index)
			temp.append(F)
			sumF+=F 
		for node2 in Nodes: 
			temp[Nodes.index(node2)]=temp[Nodes.index(node2)]/sumF
		ProbMat.append(temp)
	ProbMat=np.array(ProbMat)
	return ProbMat

def CreateListReq(Nodes,NodeObjects): 
	"""
	Returns the number of Received request for each node
	and the total number of requests
	"""
	assert len(Nodes)==len(NodeObjects), "list of nodes is not equal to NodeObject" 
	#If this assert is issued that means that the nodes collected by Latencies(Serf) are different than those
	# of Node.pk (kubectl)=> serf have failed in a node or a node have failed. 
	ListReq=[-1 for i in range(len(Nodes))]
	TotalReq=0
	for node in NodeObjects:
		assert node.name in Nodes, node.name + " is Node available in Nodes"
		ListReq[Nodes.index(node.name)]=node.req
		TotalReq+=node.req
	return ListReq,TotalReq


def Run(L,Replicas,VProximity,VImbalance,SaturationPerPod,Timeout,name="ali",
		 image="alijawad/pwsip:ver1", label="app=lat", port=80,podlabel=None):
	"""
	Deployment files creation
	"""
	label="app"+name+"=hona"
	SelectedNodes=SchdMain(L,Replicas,VProximity,VImbalance,SaturationPerPod,Timeout).Nodes
	# Returns the minimal placement to cover the active gateways
	#SelectedNodes=['fridge-1-01', 'fridge-1-02']
	Allocate(SelectedNodes,label)
	# label the selected nodes for the deployment
	Deploy(name=name, image=image, label=label, replicas=len(SelectedNodes), port=port,podlabel=podlabel)
	# create the deployment file then execute it
	print(Expose("hona-"+name))
	Nodes=PickleLoad("node.pk")
	Init_lat=kube.CalculateLatencies(Nodes)
	# save the current current latency records for further investigation
	Path="/tmp/hona-"+name+".pk"
	with open(Path, 'wb') as f:
		pk.dump(Init_lat,f, pk.HIGHEST_PROTOCOL)


def SchdMain(L,Replicas,VProximity,VImbalance,SaturationPerPod,Timeout): 
	"""
	Computes the minimal placement (initial placement algorithm) 
	"""
	latencies=PickleLoad("lat.pk")# pass from add
	NodeObjects=PickleLoad("node.pk")# pass from add
	Nodes=GetSortedNodes(latencies)
	LatMat=GetLatMat(latencies,Nodes)
	ProbMat=GetProbMat(latencies,Nodes,B=1,index=1)
	ListReq,TotalReq=CreateListReq(Nodes,NodeObjects)
	SelectedNodes,TestMat,FullTestMat=GetMinReplicasNew(ProbMat,LatMat,Nodes,SaturationPerPod,ListReq,TotalReq,L)
	Best,Len=MinPlacementTesting(VProximity,VImbalance,SaturationPerPod,Timeout,ProbMat,TestMat,FullTestMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)

	# print("New Algo")
	# print("selected solution", Best.Nodes)
	# print("Number of Tested cases: ", Len)
	print(Best.Proximity,Best.Imbalance,Best.RPP)
	return Best

def Allocate(Nodes,Label):
	return kube.AddNodesLabel(Nodes,Label)

def Deploy(name="ali", image="alijawad/pwsip:ver1", label="app=lat", replicas=3, port=80,podlabel=None):
	deploy.CheckDir()
	Name_of_File=deploy.CreateDepFile(name=name, image=image, label=label, replicas=replicas, port=port,podlabel=podlabel)
	print(kube.CreateDeployment(Name_of_File)[1])
	return

def Expose(name):
	return kube.ExposeDeployment(name)[1]

def GetUnique(TestMat,Indecies,Servers,Nodes): # optimize
	temp=TestMat[:,Indecies]
	Unique=[]
	for i in range(len(Indecies)):
		li=[] 
		for j in range(len(temp)): 
			if temp[j,i]==True: 
				if sum(temp[j])==1: 
					li.append(j)
		if len(li)!=0: 
			Unique.append((Indecies[i],li,len(li)))
	NewIndecies=[]
	NewIndecies=[item[0] for item in Unique]
	#NewIndecies=Indecies
	Unique.sort(key=lambda tup: tup[2])
	return Unique,NewIndecies

def CreateTestMat(LatMat,L):
	"""
	Create a Bool matrix of the latencies such that each latency>L will be false
	and True otherwise
	"""
	Lenlatmat=len(LatMat)
	temp=np.zeros([Lenlatmat,Lenlatmat], dtype=np.bool)
	for i in range(len(temp)):
		for j in range(len(temp[0])):
			if LatMat[i,j]>=L:
				temp[i,j]=False
			else:
				temp[i,j]=True
	return temp

def GetMinReplicasNew(ProbMat,LatMat,Nodes,SaturationPerPod,ListReq,TotalReq,L):
	Allocate=[]
	FullTestMat=CreateTestMat(LatMat,L) # Create the Mat the indicates the fast link in the Network
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)

	while len(TestMat)!= 0:
		y=np.sum(TestMat,axis=0) # How many Fast link each Server have
		ToBeAllocated=TNodes[0] # select the first node in the list (TBA)
		Potential=np.where(y==max(y))[0]
		if Potential[0]!=10: # avoid master 
			Potential=Potential[0]
		else: 
			Potential=Potential[1]
		Allocate.append(Potential)

		NIND=[]
		for b in range(len(TestMat)):
			if TestMat[b,Potential]:
				NIND.append(b)
		TestMat=np.delete(TestMat,NIND,axis=0)

	TestMat=CreateTestMat(LatMat,L)
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)
	TestMat=np.delete(TestMat,IdleGateways,axis=0)
	SelectedNodes=[Nodes[j] for j in sorted(Allocate)]
	Servers={}

	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
	Unique,NewIndecies=GetUnique(TestMat,Allocate,Servers,Nodes)
	NumberOfPods=math.ceil(TotalReq/SaturationPerPod) +1
	LinkPerServer=[]
	assert NumberOfPods<len(Nodes),"unrealistic saturation value" # we cannot create pods more than the Nodes
	if NumberOfPods>len(NewIndecies):
		diff=NumberOfPods-len(NewIndecies)
		print("Scaling up from %s by %s to meet saturation constraints (%s Total)" % (len(NewIndecies),diff,NumberOfPods))
		SumList=TestMat.sum(axis=0)
		for i in range(len(SumList)):
			LinkPerServer.append((i,SumList[i]))
		#print(NewIndecies)
		Remove=[]
		for item in LinkPerServer: 
			if item[0] in NewIndecies + [10]: 
				Remove.append(item)
		for item in Remove: 
			LinkPerServer.remove(item)
		LinkPerServer.sort(key=lambda tup: tup[1], reverse=True)
		for i in range(diff): 
			NewIndecies.append(LinkPerServer[i][0])

	SelectedNodes=[Nodes[j] for j in sorted(NewIndecies)]
	return SelectedNodes,TestMat,FullTestMat

def CheckList(Nodes,SelectedNodes): 
	"""
	Checks if all the SelectedNodes are found in Nodes
	Error checking function 
	"""
	for node in SelectedNodes: 
		if node not in Nodes:
			print("Not Found"+node)
			return False
	return True

def MakeColRemove(ProbMat,Nodes,SelectedNodes):
	"""
	Remove a column from the ProbMat
	those columns represent the nodes that are not selected
	thus the probability is equal to zero
	"""
	NIND=[]
	for i in range(len(Nodes)): 
		if Nodes[i] not in SelectedNodes: 
			NIND.append(i)
	temp=np.delete(ProbMat,NIND,axis=1)
	return temp

def CreateCaseMatrix(SelectedNodes,Nodes,ProbMat): # should be sorted Nodes and sorted SelectedNodes
	"""
	Create a new ProbMat from the unified one
	The new ProbMat illustrate the probability distribution for a single placement
	SelectedNodes SHOULD BE SORTED ACCORDING TO NODES LIST
	"""
	assert CheckList(Nodes,SelectedNodes), "Not all of the selected nodes are found in the list of nodes"
	# Remove the probabilities of the nodes that does not have a server according to the tested placement.
	assert len(ProbMat)>0, "ProbMat is an empty Matrix"
	# An error occured in the calculation of ProbMat
	# Remove the nodes that are not Selected from ProbMat
	Temp=MakeColRemove(ProbMat,Nodes,SelectedNodes)
	# Normalize
	Temp /=  Temp.sum(axis=1)[:,np.newaxis]
	return Temp

class Case:
	""" 
	class of placement cases that are being studied: 
		id			=> The sequential number of the case INT
		Nodes 		=> The Selected Nodes for the placement LIST OF OBJECTS NODE
		Proximity 	=> The Proximity Variable of the Objective function, Describes the tail latency FLOAT IN %
		Imbalance	=> The Imbalance Variable of the Objective function, Describes the load balancing FLOAT IN %
		RPP			=> The Expected load per pod ARRAY OF FLOAT
		Omega		=> The Objective function result to be maximized FLOAR ]0,1]
	""" 
	def __init__(self,Proximity,Imbalance,SelectedNodes,ProbMat,Number,RPP):
		self.id    		= Number
		self.Nodes 		= SelectedNodes
		self.Proximity	= Proximity
		self.Imbalance	= Imbalance
		self.RPP		= RPP
		self.MaxLPP		= max(RPP)
		self.MeanLPP	= statistics.mean(RPP)
		self.Omega		= 0
		self.ProbMat    = ProbMat
		self.Slow 		= []

def CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Number=0): 
	"""
	Calculates the variable Proximity (P%) and Imbalance(I%) for a placement
	represented by the SelectedNodes
	"""
	Temp=[]
	Temp= CreateCaseMatrix(SelectedNodes,Nodes,ProbMat) #ProbMat for the specific Placement
	slowReq	=	0	# Number of Slow Requests (Latency> L)
	RPP		=	[]	# Estimated Requests Per Pod
	assert len(SelectedNodes)==len(Temp[0]),"Inconsistent Temp size" # mostly a duplicate of the same node
	for i in range(len(SelectedNodes)):
		IND=Nodes.index(SelectedNodes[i])
		Total=0
		for j in range(len(Nodes)):
			assert Temp[j,i] > 0, "A probability is negative(%r), ProbMat is Wrong" % Temp[j,i]
			Req=Temp[j,i]*ListReq[j]
			Total+=Req
			if LatMat[j,IND] > L: 
				slowReq+=Req

		RPP.append(Total)
	Proximity= (1-(slowReq/TotalReq))*100 #in percent 
	std=statistics.stdev(RPP)
	Imbalance=(std/TotalReq)*100 # in percent
	Casetemp=Case(Proximity,Imbalance,SelectedNodes,Temp,Number,RPP)
	return Casetemp

def CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,Temp,L,ListReq): 
	Slow={}
	for i in range(len(Nodes)):
		Slow[i]=0
	for i in range(len(Nodes)):
		for j in range(len(SelectedNodes)):
			Req=Temp[i,j]*ListReq[i]
			IND=Nodes.index(SelectedNodes[j])
			if LatMat[i,IND] > L:
				Slow[i]+=Req
	Slow={k:v for k,v in Slow.items() if v!=0}
	Slow={k:v for k,v in sorted(Slow.items(), key=lambda item: item[1], reverse=True)}

	return Slow

def FixInitialProximity(Nodes,SelectedNodes,start,Slow,TestMat,FullTestMat,Servers,ProbMat,LatMat,ListReq,TotalReq,L,Number,VProximity,VImbalance,SaturationPerPod,Timeout): 
	Indecies=[Nodes.index(n) for n in SelectedNodes]
	#Slow=CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	ToBeAssigned=[]
	i=0
	for k,v in Slow.items():
		if i> len(Slow)/5: 
			break
		ToBeAssigned.append(k)
		i+=1
	print(ToBeAssigned)
	print(Servers)
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	print(Unique,UNodes)
	ToBeTested=GetScores(Indecies,ToBeAssigned,FullTestMat,Unique,UNodes)
	Cases=[]
	for key,li in ToBeTested.items():
		Indecies.remove(key)
		for i in li:
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			Number+=1
			SelectedNodes=[Nodes[i] for i in Indecies ]
			TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Number=Number)
			print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
			Cases.append(TempCase)
			if TestCondition(TempCase,start,VProximity,VImbalance,SaturationPerPod,Timeout)[0]: 
				return Cases,Number

			Indecies.remove(i)
		Indecies.append(key)
		Indecies.sort()
	return Cases,Number

def GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes):
	# IN(Isolated Nodes) is the nodes who have TBR as the only server
	ProperNodes=[]
	if ToBeReplaced in UNodes: 
		IN= [item for item in Unique if item[0]==ToBeReplaced][0][1]# IN(Isolated Nodes) is the nodes who have TBR as the only server
		temp=FullTestMat[IN,:]
		for i in range(len(temp[0])): 
			if i==ToBeReplaced: 
				continue
			Flag=True
			for j in range(len(temp)): 
				if not temp[j,i]:
					Flag=False
					break
			if Flag: 
				ProperNodes.append(i)
	else:
		#print(ToBeReplaced)
		ProperNodes=list(range(len(Nodes)))
		ProperNodes.remove(ToBeReplaced)
	NewProper=[]
	for node in ProperNodes: 
		if FullTestMat[NexTo,node] and node not in Indecies+[10]: 
			NewProper.append(node)
	return NewProper

def FixInitialSaturation(Nodes,SelectedNodes,start,TempCase,TestMat,FullTestMat,Servers,ProbMat,LatMat,ListReq,TotalReq,L,Number,VProximity,VImbalance,SaturationPerPod,Timeout): 
	RPPNode=[]
	ToBeTested={}
	Indecies=[Nodes.index(n) for n in TempCase.Nodes]
	for i in range(len(TempCase.Nodes)): 
		RPPNode.append((Nodes.index(TempCase.Nodes[i]),TempCase.RPP[i]))
	RPPNode.sort(key=lambda tup: tup[1])
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	NexTo=RPPNode[len(RPPNode)-1][0]
	print(Unique)
	for i in range(len(RPPNode)-1): 
		ToBeReplaced=RPPNode[i][0]
		ProperNodes=GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes)
		ToBeTested[ToBeReplaced]=ProperNodes
	Cases=[]
	print(Indecies)
	Compare=TempCase
	for key,li in ToBeTested.items():
		#print("YOHOOOO")
		#print(key)
		Indecies.remove(key)

		for i in li:
			#print(key,i)
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			Number+=1
			#print(Indecies)
			SelectedNodes=[Nodes[i] for i in Indecies ]
			Temp=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Number=Number)
			print(Temp.Proximity,Temp.Imbalance,Temp.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
			Cases.append(Temp)
			if TestCondition(Temp,start,VProximity,VImbalance,SaturationPerPod,Timeout)[0]: 
				return Cases,Number

			Indecies.remove(i)

		Indecies.append(key)
		Indecies.sort()
	return Cases,Number

def TestCondition(Case,start,VProximity,VImbalance,SaturationPerPod,timeout): 
	if Case.Proximity>VProximity and Case.MaxLPP<SaturationPerPod:#and Case.Imbalance<VImbalance:
		print("A solution was found")
		print(Case.MaxLPP,Case.Proximity,Case.Nodes) 
		return True,False
	if time.time()-start>timeout:
		print("Timeout") 
		return True,True
	return False,False

def GetMinMax2(Cases):
	"""
	Returns the Min value of Imbalance in the studied cases
	And the Max value of Proximity
	"""
	Max=Cases[0].Proximity
	Min=Cases[0].MaxLPP 
	for c in Cases: 
		if c.MaxLPP<Min:
			Min=c.MaxLPP 
		if c.Proximity>Max:
			Max=c.Proximity
	return Min,Max

def GetBestD(Cases):
	"""
	Returns the case with the Max value of Omega
	"""
	best=Cases[0] 
	for c in Cases: 
		if c.Omega>best.Omega: 
			best=c
	return best

def GetBest2(Cases,SaturationPerPod,VProximity, a=0.97):
	"""
	Compute the objective function result OMEGA
	and then returns the case with the Max value of Omega (Best Case)
		Cases 	=> A list of objects Case
		a 		=> The value Alpha defined by Hona to control the trade-off 
				   between Proximity and Imbalance
	"""
	NewCases=[]
	for case in Cases: 
		if case.MaxLPP<SaturationPerPod and case.Proximity>VProximity: 
			NewCases.append(case)
	if len(NewCases)!=0: 
		Cases=NewCases
	Min,Max=GetMinMax2(Cases)
	for c in Cases:
		c.Omega=a*(c.Proximity/Max)+(1-a)*(Min/c.MaxLPP)
	return GetBestD(Cases)

def AutoScale(VProximity,VImbalance,SaturationPerPod,Timeout,ProbMat,TestMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,NewIndecies,Number,start,Reason="Sat"): 
	Upscale=1
	NewCases=[]
	#print(NewIndecies)
	for node in Nodes: 
		if Nodes.index(node) not in NewIndecies+[10]: 
			NewIndecies.append(Nodes.index(node))
			NewIndecies.sort()
			SelectedNodes=[Nodes[i] for i in NewIndecies ]
			TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L)
			Number+=1
			NewCases.append(TempCase)
			#print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
			if TestCondition(TempCase,start,VProximity,VImbalance,SaturationPerPod,Timeout)[0]: 
				return NewCases,Number,True
			#print(Proximity,Imbalance,"Scale up "+ str(Upscale))
			NewIndecies.remove(Nodes.index(node))
	return NewCases,Number,False

def MinPlacementTesting(VProximity,VImbalance,SaturationPerPod,Timeout,ProbMat,TestMat,FullTestMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L):
	Indecies=[]
	Servers={}
	Cases=[]
	NewCases=[]
	start=time.time()
	Number=0
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
	TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Number=0)
	Cases.append(TempCase)
	if VProximity>TempCase.Proximity:
		print("Proximity Violation")
		NewCases=[]
		Slow=CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
		NewCases,Number=FixInitialProximity(Nodes,SelectedNodes,start,
							Slow,TestMat,FullTestMat,
							Servers,ProbMat,LatMat,
							ListReq,TotalReq,L,Number,
							VProximity,VImbalance,SaturationPerPod,
							Timeout)
		Cases=Cases+NewCases
		TempCase=GetBest2(Cases,SaturationPerPod,VProximity, a=.95)
	print("")
	print("Temp")
	print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
	print("")
	if SaturationPerPod<TempCase.MaxLPP:
		print("Saturation Violation")
		NewCases=[]
		NewCases,Number=FixInitialSaturation(Nodes,SelectedNodes,start,
							TempCase,TestMat,FullTestMat,
							Servers,ProbMat,LatMat,
							ListReq,TotalReq,L,Number,
							VProximity,VImbalance,SaturationPerPod,
							Timeout)
		Cases=Cases+NewCases
		TempCase=GetBest2(Cases,SaturationPerPod,VProximity, a=.95)
	print("")
	print("Temp")
	print(TempCase.Proximity,TempCase.Imbalance,TempCase.MaxLPP,[Nodes.index(node) for node in SelectedNodes])
	print("")
	NewCases=[]
	if TempCase.MaxLPP>SaturationPerPod or TempCase.Proximity<VProximity: 
		Indecies=[Nodes.index(n) for n in TempCase.Nodes]
		print("Scale up initialized")
		NewCases,Number,cond=AutoScale(VProximity,VImbalance,SaturationPerPod,
						Timeout,ProbMat,TestMat,
						LatMat,Nodes,ListReq,
						TotalReq,TempCase.Nodes,L,
						Indecies,Number,start,Reason="Sat")
		Cases=Cases+NewCases
	Best=GetBest2(Cases,SaturationPerPod,VProximity, a=.99)
	print("Final placement Size",len(Best.Nodes))
	#print(Nodes)
	return Best,Number