#!/usr/bin/python3

import numpy as np
import time
import pickle
import sys 

sys.path.append('../commands')
import kube



def PickleLoad(filename):
	"""
	Load the saved pk files
	"""
	pickledlist=[]
	with open("/tmp/"+filename, 'rb') as f:
		pickledlist = pickle.load(f)
	return pickledlist

def GetTarget(DepName):
	Deps=PickleLoad("deps.pk")
	for dep in Deps: 
		if dep.name==DepName:
			return dep
	assert False, "DepName is not found"

def GetPodName(NodeToBeReplaced,dep): 
	for pod in dep.pods: 
		if pod.node==NodeToBeReplaced: 
			return pod.id
	assert False, "Node is not in the deployment"

def Replace(NodeToBeReplaced, NodeToBeAssigned, DepName, latencies): #node1 to be replaced
	start=time.time()
	with open("/tmp/"+"oldlat"+".pk", 'wb') as f:
		pickle.dump(latencies,f, pickle.HIGHEST_PROTOCOL)
	dep=GetTarget(DepName)
	pod=GetPodName(NodeToBeReplaced,dep)
	n=len(dep.pods)
	# node1=[pod.node]
	label="app"+dep.name.split("-")[1]+"=hona"
	print("Started ADD",time.time()-start)
	print(kube.AddNodesLabel([NodeToBeAssigned],label))
	print("Started DEL",time.time()-start)
	print(kube.DeleteNodesLabel([NodeToBeReplaced],label.split("=")[0]))
	time.sleep(5)
	print("Started UP",time.time()-start)
	print(kube.ScaleDeployment(dep.name,n+1))
	print("Started DeletePod",time.time()-start)
	print(kube.DeletePod(pod))
	print("Started Down",time.time()-start)
	print(kube.ScaleDeployment(dep.name, n))

def ScaleUp(NodesToBeAssigned, DepName, latencies):
	start=time.time()
	with open("/tmp/"+"oldlat"+".pk", 'wb') as f:
		pickle.dump(latencies,f, pickle.HIGHEST_PROTOCOL)
	dep=GetTarget(DepName)
	n=len(dep.pods)
	# node1=[pod.node]
	label="app"+dep.name.split("-")[1]+"=hona"
	print("Started ADD",time.time()-start)
	print(kube.AddNodesLabel(NodesToBeAssigned,label))
	time.sleep(5)
	print("Started UP",time.time()-start)
	print(kube.ScaleDeployment(dep.name,n+len(NodesToBeAssigned)))

def ScaleDown(NodeToBeRemoved, DepName, latencies):
	start=time.time()
	with open("/tmp/"+"oldlat"+".pk", 'wb') as f:
		pickle.dump(latencies,f, pickle.HIGHEST_PROTOCOL)
	dep=GetTarget(DepName)
	pod=GetPodName(NodeToBeRemoved,dep)
	n=len(dep.pods)
	# node1=[pod.node]
	label="app"+dep.name.split("-")[1]+"=hona"
	print("Started DEL",time.time()-start)
	print(kube.DeleteNodesLabel([NodeToBeRemoved],label.split("=")[0]))
	time.sleep(5)
	print("Started DeletePod",time.time()-start)
	print(kube.DeletePod(pod))
	print("Started Down",time.time()-start)
	print(kube.ScaleDeployment(dep.name, n-1))


def ResetRPN(DepName): 
	Deps=PickleLoad("deps.pk")
	RPNNEW={}
	NewDEPS=[]
	for dep in Deps: 
		if dep.name==DepName:
			print(dep.RPN)
			for key,values in dep.RPN.items():
				RPNNEW[key]=[] 
				for t in values: 
					RPNNEW[key].append((t[0],0))
			dep.RPN=RPNNEW
		NewDEPS.append(dep)	
	with open('/tmp/deps.pk', 'wb') as f:
	 	pickle.dump(NewDEPS,f, pickle.HIGHEST_PROTOCOL)

if __name__ == "__main__":
	DepName="hona-ali"
	ResetRPN(DepName)
	Lat=PickleLoad("lat.pk")
	for n,l in Lat.items(): 
		print(n,l)
	# Deps=PickleLoad("deps.pk")
	# for d in Deps: 
	# 	for key,values in d.RPN.items(): 
	# 		for t in values: 
	# 			print(key,t[0],t[1])
# 	NodeToBeReplaced="fridge-1-02"
# 	NodeToBeAssigned="fridge-1-04"
# 	NodesToBeAssigned=["fridge-1-04","node07"]
# 	NodeToBeRemoved="node07"
# 	DepName="hona-ali"
# 	latencies=PickleLoad("lat.pk")
# 	#Replace(NodeToBeReplaced, NodeToBeAssigned, DepName, latencies)
# 	#ScaleUp(NodesToBeAssigned, DepName, latencies)
# 	ScaleDown(NodeToBeRemoved, DepName, latencies)
# 	pass