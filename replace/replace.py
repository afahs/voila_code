#!/usr/bin/python3
import statistics
import numpy as np
import time
import pandas as pd
import math
def DfPrint(df):
	print(pd.DataFrame(df))

def CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,C0,Number=0): 
	"""
	Calculates the variable Proximity (P%) and Imbalance(I%) for a placement
	represented by the SelectedNodes
	"""
	assert sorted(SelectedNodes)==SelectedNodes, "A nonsorted list of nodes was passed"
	Temp=[]
	Temp= CreateCaseMatrix(SelectedNodes,Nodes,ProbMat) #ProbMat for the specific Placement
	slowReq	=	0	# Number of Slow Requests (Latency> L)
	RPP		=	[]	# Estimated Requests Per Pod
	assert len(SelectedNodes)==len(Temp[0]),"Inconsistent Temp size " + str(len(Temp[0])) +" "+str(len(SelectedNodes)) # mostly a duplicate of the same node
	for i in range(len(SelectedNodes)):
		IND=Nodes.index(SelectedNodes[i])
		Total=0
		for j in range(len(Nodes)):
			assert Temp[j,i] > 0, "A probability is negative(%r), ProbMat is Wrong" % Temp[j,i]
			Req=Temp[j,i]*ListReq[j]
			Total+=Req
			if LatMat[j,IND] > L: 
				slowReq+=Req
				#if Req>1: 
				#	print(Nodes[j]+" "+str(j)+"->"+Nodes[IND] +" "+str(IND)+ ":"+ str(Req) +"  Latency"+str(LatMat[j,IND]) + " L0:" + str(L) )
		RPP.append(Total)
	#print([Nodes.index(node) for node in SelectedNodes])
	if TotalReq > 10: 
		Proximity= (1-(slowReq/TotalReq))*100 #in percent 
		std=statistics.stdev(RPP)
		Imbalance=(std/TotalReq)*100 # in percent
		#print(Proximity)
	else: 
		Proximity=100
		Imbalance=0
	Vc0=0
	for p in RPP: 
		if p > C0: 
			Vc0+=p-C0
	Casetemp=Case(Proximity,Imbalance,SelectedNodes,Temp,Number,RPP,Vc0,slowReq)
	return Casetemp


def CreateCaseMatrix(SelectedNodes,Nodes,ProbMat): # should be sorted Nodes and sorted SelectedNodes
	"""
	Create a new ProbMat from the unified one
	The new ProbMat illustrate the probability distribution for a single placement
	SelectedNodes SHOULD BE SORTED ACCORDING TO NODES LIST
	"""
	assert CheckList(Nodes,SelectedNodes), "Not all of the selected nodes are found in the list of nodes"
	# Remove the probabilities of the nodes that does not have a server according to the tested placement.
	assert len(ProbMat)>0, "ProbMat is an empty Matrix"
	# An error occured in the calculation of ProbMat
	# Remove the nodes that are not Selected from ProbMat
	Temp=MakeColRemove(ProbMat,Nodes,SelectedNodes)
	# Normalize
	Temp /=  Temp.sum(axis=1)[:,np.newaxis]
	return Temp



def CheckList(Nodes,SelectedNodes): 
	"""
	Checks if all the SelectedNodes are found in Nodes
	Error checking function 
	"""
	for node in SelectedNodes: 
		if node not in Nodes:
			print("Not Found"+node)
			return False
	return True

def MakeColRemove(ProbMat,Nodes,SelectedNodes):
	"""
	Remove a column from the ProbMat
	those columns represent the nodes that are not selected
	thus the probability is equal to zero
	"""
	NIND=[]
	for i in range(len(Nodes)): 
		if Nodes[i] not in SelectedNodes: 
			NIND.append(i)
	temp=np.delete(ProbMat,NIND,axis=1)
	return temp


class Case:
	""" 
	class of placement cases that are being studied: 
		id			=> The sequential number of the case INT
		Nodes 		=> The Selected Nodes for the placement LIST OF OBJECTS NODE
		Proximity 	=> The Proximity Variable of the Objective function, Describes the tail latency FLOAT IN %
		Imbalance	=> The Imbalance Variable of the Objective function, Describes the load balancing FLOAT IN %
		RPP			=> The Expected load per pod ARRAY OF FLOAT
		Omega		=> The Objective function result to be maximized FLOAR ]0,1]
	""" 
	def __init__(self,Proximity,Imbalance,SelectedNodes,ProbMat,Number,RPP,Vc0,Vl0):
		self.id    		= Number
		self.Nodes 		= SelectedNodes
		self.Proximity	= Proximity
		self.Imbalance	= Imbalance
		self.RPP		= RPP
		self.MaxLPP		= max(RPP)
		self.MeanLPP	= statistics.mean(RPP)
		self.Omega		= 0
		self.ProbMat    = ProbMat
		self.Slow 		= []
		if sum(RPP)==0: 
			self.Ep 		= 0
		else: 
			self.Ep 		= (Vl0+Vc0)*100/sum(RPP)
		self.Vc0		= Vc0
		self.Vl0		= Vl0

def ProximityViolation(Nodes,SelectedNodes,start,TempCase,TestMat,ProbMat,LatMat,ListReq,TotalReq,L,Ep0,SaturationPerPod,Timeout): 
	print("Proximity Violation")
	Best,nc=FixProximity(Nodes,SelectedNodes,start,
									TempCase,TestMat,
									ProbMat,LatMat,ListReq,
									TotalReq,L,Ep0,
									SaturationPerPod,Timeout)
	if Best.Ep<TempCase.Ep: 
		print("A replacement is Done")
		return Best,nc
	else:
		print("No Solution was found")
		return TempCase,nc


def SaturationViolation(Nodes,SelectedNodes,start,TempCase,TestMat,ProbMat,LatMat,ListReq,TotalReq,L,Ep0,SaturationPerPod,Timeout): 
	print("Saturation Violation")
	Best,nc=FixSaturation(Nodes,SelectedNodes,start,
									TempCase,TestMat,
									ProbMat,LatMat,ListReq,
									TotalReq,L,Ep0,
									SaturationPerPod,Timeout)
	if Best.Ep<TempCase.Ep:
		print("A replacement is Done")
		return Best,nc
	else:
		print("No Solution was found")
		return TempCase,nc

def FixProximity(Nodes,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,Ep0,SaturationPerPod,Timeout): 
	Servers={}
	Indecies=[]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Slow=CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,TempCase.ProbMat,L,ListReq)
	#print("Slow",Slow)
	ToBeAssigned=[]
	i=0
	for k,v in Slow.items():
		if i> len(Slow)/5: 
			break
		ToBeAssigned.append(k)
		i+=1
	#print(ToBeAssigned)
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	#print(Unique,UNodes)
	ToBeTested=GetScores(Indecies,ToBeAssigned,FullTestMat,Unique,UNodes)
	Cases=[]
	#print("TOBETESTED",ToBeTested)
	#print("FOR LOOP")
	for key,li in ToBeTested.items():
		Indecies.remove(key)
		for i in li:
			if i==10: 
				continue
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			SelectedNodes=[Nodes[i] for i in Indecies ]
			Temp=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=0)
			#print("Ep:",Temp.Ep,"Vl0:",Temp.Vl0,"Vc0",Temp.Vc0,"MaxLPP:",Temp.MaxLPP,"Total:",sum(Temp.RPP),[node for node in SelectedNodes],Temp.RPP)
			Cases.append(Temp)
			if TestCondition(Temp,start,SaturationPerPod,Timeout,Ep0)[0]: 
				return GetBest2(Cases,SaturationPerPod),len(Cases)

			Indecies.remove(i)
		Indecies.append(key)
		Indecies.sort()
	nc=len(Cases)
	Condition=TestCondition(TempCase,start,SaturationPerPod,Timeout,Ep0)[0]
	while(not Condition):
		Indecies=[Nodes.index(n) for n in TempCase.Nodes]
		Cases,Number,Condition=AutoScale(SaturationPerPod,
								Timeout,ProbMat,TestMat,
								LatMat,Nodes,ListReq,
								TotalReq,TempCase.Nodes,L,Ep0,
								Indecies,0,start,
								Reason="Proximity")
		TempCase=GetBest2(Cases,SaturationPerPod)
		nc+=len(Cases)
	Best=GetBest2(Cases,SaturationPerPod)
	return Best,nc


def FixSaturation(Nodes,SelectedNodes,start,TempCase,FullTestMat,ProbMat,LatMat,ListReq,TotalReq,L,Ep0,SaturationPerPod,Timeout): 
	RPPNode=[]
	ToBeTested={}
	Servers={}
	Indecies=[]
	TNodes=np.array(Nodes) # Set of Gateways to be allocated a nearby replica
	IdleGateways=[]
	for i in range(len(TNodes)): 
		if ListReq[i]==0: # add a threshold instead of 0
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	for i in range(len(TempCase.Nodes)): 
		RPPNode.append((Nodes.index(TempCase.Nodes[i]),TempCase.RPP[i]))
	RPPNode.sort(key=lambda tup: tup[1])
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	NexTo=RPPNode[len(RPPNode)-1][0]
	#print(Unique)
	for i in range(len(RPPNode)-1): 
		ToBeReplaced=RPPNode[i][0]
		ProperNodes=GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes)
		ToBeTested[ToBeReplaced]=ProperNodes
	Cases=[]
	#print(Indecies)
	for key,li in ToBeTested.items():
		#print("YOHOOOO")
		#print(key)
		Indecies.remove(key)

		for i in li:
			#print(key,i)
			#print(NewIndecies)
			Indecies.append(i)
			Indecies.sort()
			#print(NewIndecies)
			#print(Indecies)
			SelectedNodes=[Nodes[i] for i in Indecies ]
			Temp=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod,Number=0)
			#print("Ep:",Temp.Ep,"Vl0:",Temp.Vl0,"Vc0",Temp.Vc0,"MaxLPP:",Temp.MaxLPP,"Total:",sum(Temp.RPP),[node for node in SelectedNodes],Temp.RPP)
			Cases.append(Temp)
			if TestCondition(Temp,start,SaturationPerPod,Timeout,Ep0)[0]: 
				return GetBest2(Cases,SaturationPerPod),len(Cases)

			Indecies.remove(i)

		Indecies.append(key)
		Indecies.sort()

	if len(Cases)!=0: 
		Best=GetBest2(Cases,SaturationPerPod)
		nc=len(Cases)
	else: 
		Best=TempCase
		nc=0

	if TestCondition(Best,start,SaturationPerPod,Timeout,Ep0)[0]:
		return Best,nc
	else:
		nc=len(Cases)
		Condition=TestCondition(Best,start,SaturationPerPod,Timeout,Ep0)[0]
		while(not Condition):
			Indecies=[Nodes.index(n) for n in Best.Nodes]
			Cases,Number,Condition=AutoScale(SaturationPerPod,
									Timeout,ProbMat,TestMat,
									LatMat,Nodes,ListReq,
									TotalReq,Best.Nodes,L,Ep0,
									Indecies,0,start,
									Reason="Sat")
			if Cases!=0: 
				Best=GetBest2(Cases,SaturationPerPod)
			nc+=len(Cases)
		return Best,nc


def CalculateSlowPerGateway(Case,LatMat,Nodes,SelectedNodes,Temp,L,ListReq): 
	Slow={}
	for i in range(len(Nodes)):
		Slow[i]=0
	for i in range(len(Nodes)):
		for j in range(len(SelectedNodes)):
			Req=Temp[i,j]*ListReq[i]
			IND=Nodes.index(SelectedNodes[j])
			if LatMat[i,IND] > L:
				Slow[i]+=Req
		if Slow[i]>0.5: 
			print(Nodes[i],Slow[i])
	Slow={k:v for k,v in Slow.items() if v!=0}
	Slow={k:v for k,v in sorted(Slow.items(), key=lambda item: item[1], reverse=True)}

	return Slow

def GetUnique(TestMat,Indecies,Servers,Nodes): # optimize
	temp=TestMat[:,Indecies]
	Unique=[]
	for i in range(len(Indecies)):
		li=[] 
		for j in range(len(temp)): 
			if temp[j,i]==True: 
				if sum(temp[j])==1: 
					li.append(j)
		if len(li)!=0: 
			Unique.append((Indecies[i],li,len(li)))
	NewIndecies=[]
	NewIndecies=[item[0] for item in Unique]
	#NewIndecies=Indecies
	Unique.sort(key=lambda tup: tup[2])
	return Unique,NewIndecies


def GetScores(Indecies,ToBeAssigned,FullTestMat,Unique,UNodes):
	# ADD=[]
	# for i in range(len(FullTestMat)): 
	# 	if i not in Indecies: 
	# 		ADD.append(i)
	scores=[]
	ToBeTested={}
	#print(len(ADD))
	NotUnique=[i for i in Indecies if i not in UNodes] # those nodes can be replaced without affecting the Proximity
	#print(NotUnique)

	# if NotUnique!=[]:
	# 	for node in NotUnique: 

	temp=FullTestMat[ToBeAssigned,:] #ToBeAssigned are the nodes that have slow latency
	y=np.sum(temp,axis=0) # for every available gateway node, get how many fast link the gateway can provide for the TBA
	for i in range(len(y)):
		if i not in Indecies and y[i]!=0: # Selected nodes are excluded, and gateways with 0 fast links
			scores.append((i,y[i])) # A score for every possible gateway
	scores.sort(key=lambda tup: tup[1], reverse=True) 
	Newscores=[]
	for i in range(len(scores)): 
		if FullTestMat[ToBeAssigned[0],scores[i][0]] and i<11: # select only the first 10 nodes that can serve the first TBA 
			Newscores.append(scores[i][0])
	for i in NotUnique: # Try to replace an unimportant node by one of the nodes with high score
		ToBeTested[i]=Newscores
	#print(scores)
	#print(Newscores)
	return ToBeTested # returns the potential placement

def TestCondition(Case,start,SaturationPerPod,timeout,Ep0): 
	#print(Case.Ep)
	if Case.Ep<Ep0:
		print("A solution was found, time: "+str(time.time()-start) +" s")  
		return True,False
	if time.time()-start>timeout:
		print("Timeout") 
		return True,True
	return False,False

# def GetMinMax2(Cases):
# 	"""
# 	Returns the Min value of Imbalance in the studied cases
# 	And the Max value of Proximity
# 	"""
# 	Max=Cases[0].Proximity
# 	Min=Cases[0].MaxLPP 
# 	for c in Cases: 
# 		if c.MaxLPP<Min:
# 			Min=c.MaxLPP 
# 		if c.Proximity>Max:
# 			Max=c.Proximity
# 	return Min,Max

# def GetBestD(Cases):
# 	"""
# 	Returns the case with the Max value of Omega
# 	"""
# 	best=Cases[0] 
# 	for c in Cases: 
# 		if c.Omega>best.Omega: 
# 			best=c
# 	return best

def GetBest2(Cases,SaturationPerPod):
	"""
	Returns best Ep0
	"""
	MinEp=100
	BestCase=Cases[0]
	for case in Cases:
		case.Vl0=sum(case.RPP)*(100-case.Proximity)/100
		case.Vc0=0
		for p in case.RPP: 
			if p > SaturationPerPod: 
				case.Vc0+=p-SaturationPerPod
		if sum(case.RPP)==0: 
			case.Ep=0 
		else: 
			case.Ep=(case.Vc0+case.Vl0)*100/sum(case.RPP)
		#print("Vl0",case.Vc0,"Vc0",case.Vc0,"Ep",case.Ep,"total",sum(case.RPP),case.Nodes)
		if case.Ep<MinEp: 
			MinEp=case.Ep
			BestCase=case
	return BestCase

def AutoScale(SaturationPerPod,Timeout,ProbMat,TestMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Ep0,NewIndecies,Number,start,Reason="Sat"): 
	Upscale=1
	NewCases=[]
	#print(NewIndecies)
	for node in Nodes: 
		if (Nodes.index(node) not in NewIndecies) and (node!="fridge01"): 
			NewIndecies.append(Nodes.index(node))
			NewIndecies.sort()
			SelectedNodes=[Nodes[i] for i in NewIndecies ]
			TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod)
			Number+=1
			NewCases.append(TempCase)
			#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[node for node in SelectedNodes],TempCase.RPP)
			if TestCondition(TempCase,start,SaturationPerPod,Timeout,Ep0)[0]: 
				return NewCases,Number,True
			#print(Proximity,Imbalance,"Scale up "+ str(Upscale))
			NewIndecies.remove(Nodes.index(node))
	return NewCases,Number,False

def GetProperNodes(ToBeReplaced,Indecies,Nodes,FullTestMat,NexTo,Unique,UNodes):
	# IN(Isolated Nodes) is the nodes who have TBR as the only server
	ProperNodes=[]
	if ToBeReplaced in UNodes: 
		IN= [item for item in Unique if item[0]==ToBeReplaced][0][1]# IN(Isolated Nodes) is the nodes who have TBR as the only server
		temp=FullTestMat[IN,:]
		for i in range(len(temp[0])): 
			if i==ToBeReplaced: 
				continue
			Flag=True
			for j in range(len(temp)): 
				if not temp[j,i]:
					Flag=False
					break
			if Flag: 
				ProperNodes.append(i)
	else:
		#print(ToBeReplaced)
		ProperNodes=list(range(len(Nodes)))
		ProperNodes.remove(ToBeReplaced)
	NewProper=[]
	for node in ProperNodes: 
		if FullTestMat[NexTo,node] and node not in Indecies and node!=10:
			#print(node,type(node))
			NewProper.append(node)
	return NewProper

def ProvisioningViolation(TempCase,ProbMat,LatMat,TestMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Ep0,LPC,SaturationPerPod,Timeout): 
	print("Checking for a scale down possibility",len(TempCase.Nodes)-(TotalReq/SaturationPerPod))
	Best,nc=CheckscaleDown(TempCase,ProbMat,LatMat,TestMat,Nodes,ListReq,
								TotalReq,SelectedNodes,L,Ep0,LPC,
								SaturationPerPod,Timeout)
	if Best==TempCase: 
		print("Didn't scale down")
	else:
		print("Scale done success")
	return Best,nc
	# 				Change[0]="Over"
	# 				Change[1]="Scale Down"
	# 				Changes.loc[cycle]=Change
	# 				Changes.to_pickle("./Changes.pk")
	# 				time.sleep(sleep)
	# 				return Best


def CheckscaleDown(Current,ProbMat,LatMat,FullTestMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Ep0,LPC,SaturationPerPod=50,Timeout=1): 
	DownScale=3
	Cases=[]
	IdleGateways=[]
	Servers={}
	Indecies=[]
	ActiveNodes=GetActiveNodes(LPC,3)
	lenlpc=len(LPC)
	last=sum(list(LPC[lenlpc-1]))
	pr2=0
	pr1=0
	if lenlpc>2:
		pr2=sum(list(LPC[lenlpc-3]))
	if lenlpc>1:
		pr1=sum(list(LPC[lenlpc-1]))

	if (pr2-last>SaturationPerPod/2 and pr2>SaturationPerPod) or last==0 : 
		DownScale=math.floor(len(Current.Nodes)-(TotalReq/SaturationPerPod)-2)
	else: 
		DownScale=1
	if pr1<last: 
		DownScale=0
	# for i in ActiveNodes: 
	# 	print(Nodes[i],end=",")
	# print("")
	for i in range(len(Nodes)): 
		if i not in ActiveNodes: 
			IdleGateways.append(i)

	TestMat=np.delete(FullTestMat,IdleGateways,axis=0)
	#DfPrint(TestMat)
	for node in SelectedNodes: 
		Index=Nodes.index(node)
		Servers[Index]=sum(TestMat[:,Index])
		Indecies.append(Index)
	Unique,UNodes=GetUnique(TestMat,Indecies,Servers,Nodes)
	#print(Unique,UNodes)
	newSelected=SelectedNodes
	i=0
	Number=0
	start=time.time()
	print("DownScale",DownScale)
	while i<DownScale:
		i+=1
		Cases=[]
		if len(newSelected)<=2: 
			break
		for node in newSelected:
			if Nodes.index(node) not in UNodes: 
				Number+=1
				newNodes=ExcludeNode(newSelected,node) 
				TempCase=CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,newNodes,L,SaturationPerPod)
				#print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",sum(TempCase.RPP),[node for node in SelectedNodes],TempCase.RPP)
				Cases.append(TempCase)

				#print(TempCase.Proximity,TempCase.MaxLPP)
				#print(TempCase.__dict__)
		if len(Cases)!=0: 
			Best=GetBest2(Cases,SaturationPerPod)
			print(Best.Proximity,Best.MaxLPP)
			if TestCondition(Best,start,SaturationPerPod,10,Ep0):
				#print(Current.Proximity,Current.RPP) 
				print(Best.Ep,Best.RPP)
				#print("reach!")
				Current=Best
				newSelected=Best.Nodes
			else: 
				break
		else: 
			break


	return Current,Number

def ExcludeNode(SelectedNodes,node): 
	newNodes=[]
	for n in SelectedNodes: 
		if n !=node: 
			newNodes.append(n)
	return newNodes

def GetActiveNodes(LPC,NC): 
	lenlpc=len(LPC)
	LastLPC=[]
	for i in range(NC):
		index=len(LPC)-i-1
		if index> 0: 
			LastLPC.append(list(LPC[index]))
	ActiveNodes=[]
	for l in LastLPC: 
		for i in range(len(l)): 
			if l[i]!=0:
				if i not in ActiveNodes: 
					ActiveNodes.append(i) 
	return ActiveNodes