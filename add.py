#!/usr/bin/python3

import sys
import pickle
import os.path
import argparse
import time
sys.path.append('./commands')
sys.path.append('./deploy')
import schedule as schd
import kube

# kube.PrintNodes(NodesObjects)
# kube.PrintDeps(Deps)
# kube.PrintLoadsPods(Pods)
# kube.PrintLatencies(latencies)
# kube.PrintPods(Pods)

def Gettime(start,phrase):
	current=time.time()
	print(current-start,phrase)
	return current

def PickleNodes(Nodes,Pods,Deps,latencies):
	"""
	Save the collected info at /temp
	"""
	with open('/tmp/node.pk', 'wb') as f:
		pickle.dump(Nodes,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/pods.pk', 'wb') as f:
		pickle.dump(Pods,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/deps.pk', 'wb') as f:
		pickle.dump(Deps,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/lat.pk', 'wb') as f:
		pickle.dump(latencies,f, pickle.HIGHEST_PROTOCOL)

def parseCliOptions():
	"""
	input parameters 
	"""
	parser = argparse.ArgumentParser()

	parser.add_argument( '--VP', #imp
		dest       = 'VProximity', 
		type       = float,
		default    = 99,
		help       = 'Proximity threshold',
	)

	parser.add_argument( '--SPP', #imp
		dest       = 'SaturationPerPod', 
		type       = int,
		default    = 100,
		help       = 'Max number of packets a pod can handle',
	)
	
	parser.add_argument( '--L', #imp
		dest       = 'L', 
		type       = float,
		default    = 20,
		help       = 'Application latency threshold',
	)
	options=parser.parse_args()
	return options.__dict__

def FindPlacement(L=40,VProximity=99.9,SaturationPerPod=150,Timeout=1,name="ali"):
	start=time.time()
	#best=schd.SchdMain(L,5,VProximity,10,SaturationPerPod,Timeout)
	schd.Run(L,5,VProximity,10,SaturationPerPod,Timeout,name=name)
	start=Gettime(start," Time of the new Algo")
	
if __name__ == "__main__":

	options = parseCliOptions()
	VProximity=options['VProximity']
	SaturationPerPod=options['SaturationPerPod']
	L=options['L']
	print(VProximity,SaturationPerPod,L)

	##move to the schd
	if os.path.exists('/tmp/node.pk') and os.path.exists('/tmp/pods.pk') and os.path.exists('/tmp/deps.pk'): 
		NodesObjects			=schd.PickleLoad("node.pk")
		Pods					=schd.PickleLoad("pods.pk")
		Deps					=schd.PickleLoad("deps.pk")
	else: 
		NodesObjects,Pods,Deps	=kube.Create()
	if os.path.exists('/tmp/lat.pk'): 
		latencies				=schd.PickleLoad("lat.pk")
	else: 
		latencies				=kube.CalculateLatencies(Nodes)

	Nodes=schd.GetSortedNodes(latencies)
	##move to the schd

	FindPlacement(L=20,VProximity=99.0,SaturationPerPod=100,Timeout=1) # fix the parameters here
	#PickleNodes(NodesObjects,Pods,Deps,latencies)	
	
