#!/usr/bin/python3

import sys
from importlib import reload
import time
import argparse
import pickle
import pandas as pd
import numpy as np

sys.path.append('../commands')
sys.path.append('../replace')
import kube
import prepare
import replace as re
import execute as ex

def parseCliOptions():
	parser = argparse.ArgumentParser()

	parser.add_argument( '--Cycle', #imp
		dest       = 'Cycle', 
		type       = int,
		default    = 0,
		help       = 'Cycle time',
	)

	parser.add_argument( '--SaturationPerPod', #imp
		dest       = 'SaturationPerPod', 
		type       = int,
		default    = 100,
		help       = 'Max number of packets a pod can handle',
	)

	options=parser.parse_args()
	return options.__dict__

def Renameold(): 
	os.rename("/tmp/deps.pk","/tmp/deps_old.pk")

def PickleLoad(filename):
	"""
	Load the saved pk files
	"""
	pickledlist=[]
	with open("/tmp/"+filename, 'rb') as f:
		pickledlist = pickle.load(f)
	return pickledlist

def GetTime(start,Phrase): 
	print(Phrase+": "+str(time.time()-start))
	return time.time()

def CalculateLoadPerNode(RPN): 
	LoadPerNode={}
	for Node,Load in RPN.items():
		LoadPerNode[Node]=0
		for IP,Number in Load: 
			LoadPerNode[Node]+= Number
	return LoadPerNode

def CountDown(x): 
	for remaining in range(int(x), 0, -1):
		sys.stdout.write("\r")
		sys.stdout.write("{:2d} seconds remaining.".format(remaining))
		sys.stdout.flush()
		time.sleep(1)

def GetLoadPerCycle(Deps,DepsOld,svc="hona-ali"): 
	for d in Deps: 
		if d.name==svc: 
			Dnew=d
			break
	for d in DepsOld: 
		if d.name==svc: 
			Dold=d
			break
	LoadPerNodeOld=CalculateLoadPerNode(Dold.RPN)
	LoadPerNodeNew=CalculateLoadPerNode(Dnew.RPN)
	LoadPerNodeCycle={}
	for Node in LoadPerNodeOld.keys(): 
		LoadPerNodeCycle[Node]=LoadPerNodeNew[Node]-LoadPerNodeOld[Node]
	return LoadPerNodeCycle,[pod.node for pod in Dnew.pods]


def PickleNodes():
	print("Running data collection")
	start1=time.time()
	Nodes,Pods,Deps=kube.Create()
	start=GetTime(start1,"Cluster info")
	latencies=kube.CalculateLatencies(Nodes)
	start=GetTime(start,"lat info")
	DepsOld=PickleLoad("deps.pk")
	# Nodes=PickleLoad("node.pk")#temp
	# Pods=PickleLoad("pods.pk")#temp
	# Deps=PickleLoad("deps.pk")#temp
	# latencies=PickleLoad("lat.pk")#temp
	with open('/tmp/node.pk', 'wb') as f:
		pickle.dump(Nodes,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/pods.pk', 'wb') as f:
		pickle.dump(Pods,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/deps.pk', 'wb') as f:
		pickle.dump(Deps,f, pickle.HIGHEST_PROTOCOL)
	with open('/tmp/lat.pk', 'wb') as f:
		pickle.dump(latencies,f, pickle.HIGHEST_PROTOCOL)
	start=GetTime(start,"pickling info")
	LoadPerNodeCycle,SelectedNodes=GetLoadPerCycle(Deps,DepsOld,svc="hona-ali")
	start=GetTime(start,"LPC info")
	kube.PrintDeps(Deps)
	kube.PrintPods(Pods)
	start=GetTime(start,"Printing info")
	start=GetTime(start1,"Total")
	SelectedNodes.sort()
	return LoadPerNodeCycle,SelectedNodes,latencies



def WriteCsvRow(f,row):
	str1=""
	size=len(row)
	for i in range(size): 
		if i ==size-1: 
			str1+=str(row[i])+"\n"
		else:
			str1+=str(row[i])+","
	f.write(str1)

def FillRow(dfload,LoadPerNodeCycle,i): 
	for Node,Load in LoadPerNodeCycle.items(): 
		dfload.loc[i,Node]=Load

def GetSortedNodes(LoadPerNodeCycle):
	"""
	Returns a list of Sorted Nodes
	"""
	Nodes=[]
	for n in LoadPerNodeCycle.keys():
		Nodes.append(n)
	Nodes=sorted(Nodes)
	return Nodes

def dftoLPC(dfload):
	for i in range(len(dfload)):
		print(dfload.loc[i])

def DfPrint(df):
	print(pd.DataFrame(df))


def CalculateActualEP(tolerance,SaturationPerPod,Actual,TempCase,TotalReq):
	AVc0=0
	for P in TempCase.RPP: 
		if P> Actual: 
			AVc0+=(P-Actual)
	if TotalReq!=0: 
		AEp=(TempCase.Vl0 + AVc0)*100/TotalReq
	else: 
		AEp=0
	return AEp

def Check(cycle,LoadPerNodeCycle,LPC,latencies,SelectedNodes,f,g,Previous,L=25, Ep0=0.5,SaturationPerPod=50,Safety=0.2,Timeout=10): 
	tolerance=Safety #[0,1]
	VProximity=0 # TEMP
	VImbalance=0 # TEMP
	Actual=SaturationPerPod #The real saturation of the pod
	SaturationPerPod=SaturationPerPod*(1-tolerance) # The passed saturation for provisioning
	DepName="hona-ali"
	Nodes=GetSortedNodes(LoadPerNodeCycle)
	LatMat=prepare.GetLatMat(latencies,Nodes)
	ProbMat=prepare.GetProbMat(latencies,Nodes,B=1,index=1)
	TestMat=prepare.CreateTestMat(LatMat,L)
	ListReq=[LoadPerNodeCycle[Node] for Node in Nodes]
	TotalReq=sum(ListReq)
	Change="None"
	Violation="None"
	Studied=0
	TempCase=re.CalculateObjectiveVariables(ProbMat,LatMat,Nodes,ListReq,TotalReq,SelectedNodes,L,SaturationPerPod)
	AEp=CalculateActualEP(Safety,SaturationPerPod,Actual,TempCase,TotalReq)
	print("Ep:",TempCase.Ep,"Vl0:",TempCase.Vl0,"Vc0",TempCase.Vc0,"MaxLPP:",TempCase.MaxLPP,"Total:",TotalReq,[Nodes.index(node) for node in SelectedNodes])
	if TempCase.Vc0>0.5: 
		print("Actual",Actual,"Passed",SaturationPerPod,"Actual E",AEp, "Calculated Ep",TempCase.Ep)
	start=time.time()
	if Ep0<TempCase.Ep and TempCase.Vl0 > TempCase.Vc0 and TempCase.Vl0>1.1: 
		Best,nc=re.ProximityViolation(Nodes,SelectedNodes,start,TempCase,TestMat,ProbMat,LatMat,ListReq,TotalReq,L,Ep0,SaturationPerPod,Timeout)
		Violation="Proximity"
		Studied=nc	
	else:
		if Ep0<TempCase.Ep and TempCase.Vl0 < TempCase.Vc0:
			Best,nc=re.SaturationViolation(Nodes,SelectedNodes,start,TempCase,TestMat,ProbMat,LatMat,ListReq,TotalReq,L,Ep0,SaturationPerPod,Timeout)
			Violation="Saturation"
			Studied=nc
		else: 
			if len(TempCase.Nodes)> (TotalReq/SaturationPerPod)+2 and not Previous:
				Violation="Provisioning"
				Best,nc=re.ProvisioningViolation(TempCase,ProbMat,LatMat,TestMat,Nodes,ListReq,TotalReq,SelectedNodes,L,Ep0,LPC,SaturationPerPod,Timeout)
				Studied=nc
				#Best=TempCase
			else: 
				Best=TempCase
	
	if Best.Nodes!=TempCase.Nodes: 
		if len(Best.Nodes)==len(TempCase.Nodes): 
			print("Execute a Replacement")
			NodesToBeAssigned=[]
			NodeToBeReplaced=""
			for node in Best.Nodes: 
				if node not in TempCase.Nodes: 
					NodesToBeAssigned.append(node)
					break
			for node in TempCase.Nodes: 
				if node not in Best.Nodes:
					NodeToBeReplaced=node
					break
			Change="Replace"
			if NodeToBeReplaced=="" or NodesToBeAssigned==[]: 
				print("something went wrong same nodes")
			else: 
				Change="Replace"
				print(NodesToBeAssigned," will be  Added")
				assert "fridge01" not in NodesToBeAssigned, "Master Selected"
				ex.ScaleUp(NodesToBeAssigned, DepName, latencies)
				print("Execute Scale Down")
				print(NodeToBeReplaced +" will be removed")
				ex.ScaleDown(NodeToBeReplaced, DepName, latencies,len(TempCase.Nodes)+len(NodesToBeAssigned))
		
		if len(Best.Nodes)>len(TempCase.Nodes): #scaled up 
			print("Execute Scale Up")
			NodesToBeAssigned=[]
			NodeToBeReplaced=""
			for node in Best.Nodes: 
				if node not in TempCase.Nodes: 
					NodesToBeAssigned.append(node)
			for node in TempCase.Nodes: 
				if node not in Best.Nodes:
					NodeToBeReplaced=node
					break
			if NodeToBeReplaced=="": 
				print(NodesToBeAssigned," will be  Added")
				assert "fridge01" not in NodesToBeAssigned, "Master Selected"
				ex.ScaleUp(NodesToBeAssigned, DepName, latencies)
				Change="Up"
			else:
				Change="Up/Replace"
				print(NodesToBeAssigned," will be  Added")
				assert "fridge01" not in NodesToBeAssigned, "Master Selected"
				ex.ScaleUp(NodesToBeAssigned, DepName, latencies)
				print("Execute Scale Down")
				print(NodeToBeReplaced +" will be removed")
				ex.ScaleDown(NodeToBeReplaced, DepName, latencies,len(TempCase.Nodes)+len(NodesToBeAssigned))
		NodesToBeRemoved=[]
		if len(Best.Nodes)<len(TempCase.Nodes):
			Change="Down" 
			print("Execute Scale Down")
			for node in TempCase.Nodes: 
				if node not in Best.Nodes: 
					NodesToBeRemoved.append(node)
			print(NodesToBeRemoved ," will be removed")
			ex.ScaleDowns(NodesToBeRemoved, DepName, latencies,len(TempCase.Nodes))
		ex.ResetRPN(DepName)
	Utilization=TotalReq*100/(len(SelectedNodes)*Actual)
	row=[cycle,len(TempCase.Nodes),TempCase.Ep,AEp,TempCase.Vl0,TempCase.Vc0,TempCase.MaxLPP,Utilization,TotalReq,Violation,Change,Studied,Ep0,L,SaturationPerPod]
	row2=[cycle]+[node for node in SelectedNodes]
	print("Ep:",Best.Ep,"Vl0:",Best.Vl0,"Vc0",Best.Vc0,"MaxLPP:",Best.MaxLPP,"Total:",TotalReq,[Nodes.index(node) for node in Best.Nodes])
	WriteCsvRow(f,row)
	WriteCsvRow(g,row2)
	if Ep0<TempCase.Ep: 
		return True
	else: 
		return False



if __name__ == "__main__":
	options = parseCliOptions()
	t=options['Cycle']
	Ep0=0.5
	C0=50
	S0=0.1 #Safety[0,1]
	L0=15
	T0=5
	Bias=15
	Tol=0.2
	g=18
	n=22
	filename1="_G_"+str(g)+"_n_"+str(n)+"_L0_"+str(L0)+"_C0_"+str(C0)+"_Bias_"+str(Bias)+"_Tol_"+str(Tol)
	f = open("./res/res"+filename1+".csv", "w")
	g = open("./res/nodes"+filename1+".csv", "w")
	row=["Cycle","Size","Ep","AEp","Vl0","Vc0","MaxLPP","Utilization","TotalReq","Violation","Change","NCases","Ep0","L0","C0"]
	WriteCsvRow(f,row)

	TC=2*60
	print("\nCycle 0")
	print("______________________________________________________________________")
	start=time.time()
	LoadPerNodeCycle,SelectedNodes,latencies=PickleNodes()
	columns=sorted([node for node in LoadPerNodeCycle.keys()])
	LPCRow=[LoadPerNodeCycle[Node] for Node in columns]
	dfload= pd.DataFrame(columns=columns)
	#re.CheckDeps(99.5,5,1,1)
	dfload.loc[0]=[0 for i in range(len(columns))]
	FillRow(dfload,LoadPerNodeCycle,0)
	print(dfload)
	i=0
	LPC=np.array([LPCRow])
	Previous=Check(i,LoadPerNodeCycle,LPC,latencies,SelectedNodes,f,g,False,L=L0,Ep0=Ep0,SaturationPerPod=C0,Safety=S0,Timeout=T0)
	print(-start+time.time())
	print(start-time.time()+TC)
	CountDown(start-time.time()+TC)
	while True:
		i+=1
		reload(kube)
		print("\nCycle "+str(i)) 
		print("______________________________________________________________________")
		print("")
		#time.sleep(t)
		start=time.time()
		LoadPerNodeCycle,SelectedNodes,latencies=PickleNodes()
		# LoadPerNodeCycle["fridge02"]=10
		# LoadPerNodeCycle["node01"]=10
		# LoadPerNodeCycle["fridge-1-02"]=7
		# LoadPerNodeCycle["fridge-2-01"]=8
		# LoadPerNodeCycle["fridge-1-05"]=8
		# LoadPerNodeCycle["fridge-1-01"]=8
		# LoadPerNodeCycle["fridge05"]=7
		# LoadPerNodeCycle["node07"]=8
		# LoadPerNodeCycle["fridge-1-04"]=8
		# LoadPerNodeCycle["fridge-1-03"]=10

		dfload.loc[i]=[0 for i in range(len(columns))]
		FillRow(dfload,LoadPerNodeCycle,i)
		LPCRow=[LoadPerNodeCycle[Node] for Node in columns]
		LPC=np.append(LPC,np.array([LPCRow]),axis=0)
		print(dfload)
		Previous=Check(i,LoadPerNodeCycle,LPC,latencies,SelectedNodes,f,g,Previous,L=L0,Ep0=Ep0,SaturationPerPod=C0,Safety=S0,Timeout=T0)
		
		if i==29: 
			break
		print(-start+time.time())
		print(start-time.time()+TC)
		CountDown(start-time.time()+TC)

