#!/usr/bin/python3
import numpy as np 
import math 

def GetLatency(Node1,Node2,latencies):
	"""
	Returns the latency between Node1 and Node2
	"""
	for k,v in latencies[Node1]: 
		if k==Node2: 
			return v 
	print("latency not found, Returning 0")
	return 0

def GetLatMat(latencies,Nodes):
	"""
	Create a Latency mat as np array
	""" 
	l=[]
	for node1 in Nodes:
		temp=[]
		for node2 in Nodes:
			temp.append(GetLatency(node1,node2,latencies))
		l.append(temp)
	LatMat=np.array(l)
	return LatMat


def GetProbMat(latencies,Nodes,B=1,index=1):
	"""
	Create a unified Probability that will be used with all the selected placement: 
		latencies 	=> Inter-node Latencies DICT
		Nodes 		=> list of SORTED nodes names LIST OF STRINGS
		B 			=> The value Beta in Proxy-mity equation FLOAT ]0,1]
		index		=> Decides what function to be used to calculate function F INT {1,2,3}
	"""
	ProbMat=[]
	for node1 in Nodes: 
		temp=[]
		sumF=0
		for node2 in Nodes: 
			l=GetLatency(node1,node2,latencies)
			#Apply proxy-mity Routing function
			F=CalculateF(l,B=B,index=index)
			temp.append(F)
			sumF+=F 
		for node2 in Nodes: 
			temp[Nodes.index(node2)]=temp[Nodes.index(node2)]/sumF
		ProbMat.append(temp)
	ProbMat=np.array(ProbMat)
	return ProbMat

def CalculateF(x,B=1,index=1):
	"""
	Calculate the function defined in Proxy-mity:
		x			=> The latency between the two tested nodes
		B 			=> The value Beta in Proxy-mity equation FLOAT ]0,1]
		index		=> Decides what function to be used to calculate function F INT {1,2,3}

	"""
	assert x>0, "the latency value (%r) is negative or zero" % x
	if index==1: # Choose the Exponential function
		return math.exp(-B*x)
	if index==2: # Choose the proportional function
		return 1/(B*x)
	if index==3: # Choose the Degree function
		return 1/math.pow(x,B)


def CreateTestMat(LatMat,L):
	"""
	Create a Bool matrix of the latencies such that each latency>L will be false
	and True otherwise
	"""
	Lenlatmat=len(LatMat)
	temp=np.zeros([Lenlatmat,Lenlatmat], dtype=np.bool)
	for i in range(len(temp)):
		for j in range(len(temp[0])):
			if LatMat[i,j]>=L:
				temp[i,j]=False
			else:
				temp[i,j]=True
	return temp

